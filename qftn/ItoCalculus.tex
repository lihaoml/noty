\section{\Ito Calculus}

\begin{concept}{\Ito Process}
A \Ito process $X$ is a continuous process ($X_t: t \geq 0$) such that
$X_t$ can be written as
\begin{equation}
X_t = X_0 + \int_0^t \mu_s ds + \int_0^t \sigma_s dW_s
\end{equation}
where $\mu$ and $\sigma$ are random $\mathcal{F}$-previsible processes such that
$\int_0^t(\sigma_s^2 + \lvert\mu_s\rvert)ds$ is finite. And $W_t$ is a standard Wiener process.
The differential of $X_t$ can be written as
\begin{equation}
d X_t = \mu_t dt + \sigma_t dW_t
\end{equation} 
\end{concept}

Newtonian calculus has the chain rule, product rule, integration by parts etc., to manipulate the Newtonian differentials.
Can we apply the same to stochastic differentials? Let us look at the Taylor expansion of $f(t)$ for some smooth function $f$:
\begin{equation}
f(t+\Delta t) = f'(t)\Delta t + \frac{1}{2} f''(t) \Delta t^2 + \frac{1}{3!} f^{(3)}(t) \Delta t^3 + \dots 
\end{equation}
For any smooth function $f$ of $t$, no matter how weird its shape is, the infiniesimal segment of f is a straight line.
so we write ($df(t) = f'(t) dt : dt \rightarrow 0$) because $dt^2$ and higher-order terms are zero. 
Similarly, for a continuously differentiable function $f$ of a process $C_t$ which is itself differentiable with respect to $t$,
applying chain rule we have
\begin{equation}
\label{eqn:firstOrder}
df(C(t)) = f'(C) C'(t) dt = f'(C) dC(t)
\end{equation}

However, \eqref{eqn:firstOrder} does not apply to stochastic process because the Wiener process $W_t$ is differentiable nowhere with respect to $t$.
The rule of thumb is, $dW \cdot dW = dt$ and therefore the second order term cannot be ignored. 
So we have the stochastic calculus version of the change of variable and chain rule:

\begin{concept}{It\^{o}'s Lemma: Simplest Form}
If $X$ is a stochastic process satisfying $dX_t = \mu_t dt + \sigma_t dW_t$,
and $f$ is a deterministic continuously twice differentiable function.
Then $Y_t := f(X_t)$ is also a stochastic process satisfying
\begin{eqnarray}
d Y_t & = & f'(X_t) dX + \frac{1}{2} f''(X_t) dX^2 \\
      & = & (\mu f'(X_t) + \frac{1}{2} \sigma^2 f''(X_t)) dt +
        (\sigma f'(X_t)) dW_t
\end{eqnarray}
\end{concept}

\begin{concept}{It\^{o}'s Lemma: $Y_t = f(t, X_t)$}
For a twice differentiable function $f$ of $(t, X_t)$,
the process $Y_t = f(t, X_t)$ satisfies,
\begin{equation}
d Y_t = \frac{\partial f}{\partial t} dt + \frac{\partial f}{\partial X} dX + \frac{1}{2}\frac{\partial^2f}{\partial x^2} dX^2
\end{equation}
Expanding it with $dX_t = \mu_t dt + \sigma_t dW_t$ we get:
\begin{equation}
d Y_t = (\frac{\partial f}{\partial t} + \mu_t \frac{\partial f}{\partial X} + \frac{\sigma_t^2}{2} \frac{\partial^2f}{\partial x^2} ) dt 
+ \sigma_t \frac{\partial f}{\partial X} dW_t
\end{equation}
\end{concept}

\begin{concept}{\Ito Product Rule}
In short, \Ito product rule is
\begin{equation}
d (XY) = YdX + XdY + dX dY
\end{equation}
where $X$ and $Y$ are two stochastic processes. Equivalently,
\begin{equation}
\label{eqn:itoprodProduct}
\frac{d (XY)}{XY}  = \frac{dX}{X} + \frac{dY}{Y} + \frac{dX dY}{XY}
\end{equation}
\Ito quotient rule can be inferred:
\begin{equation}
\label{eqn:itoquotient}
d \left(\frac{X}{Y}\right) = \frac{dX}{Y} + X d \frac{1}{Y} + dX d\frac{1}{Y}.
\end{equation}
Substituting
\begin{equation}
d \left(\frac{1}{Y}\right) = -\frac{dY}{Y^2} + \frac{dY^2}{Y^3}
\end{equation} 
into \eqref{eqn:itoquotient} we have
\begin{equation}
\frac{d (X / Y)}{ X/Y } = \frac{dX}{X} - \frac{dY}{Y} + \left(\frac{dY}{Y}\right)^2 - \frac{dXdY}{XY}
\label{eqn:itoquotProduct}
\end{equation}
\end{concept}
In trading language \eqref{eqn:itoprodProduct} reads
\begin{itemize}[noitemsep,nolistsep]
\item long X and long Y,
\item long the correlation between X and Y.
\end{itemize}
And \eqref{eqn:itoquotProduct} reads 
\begin{itemize}[noitemsep,nolistsep]
\item long X and short Y,
\item long the volatility of Y,
\item short the correlation between X and Y.
\end{itemize}

In ordinary calculus, the formula of integration by parts can be derived by integration the product rule of differentiation.
It's compact form is
\begin{equation}
uv = \int udv + \int vdu 
\end{equation}
In stochastic calculus, the integration by parts formula differs by
the inclusion of a quadratic covariation term.
\begin{concept}{Integration by Parts}
\begin{equation}
X_tY_t = X_0 Y_0 + \int_0^t X_sdY_s +  \int_0^t Y_s dX_s + [X_t, Y_t]
\end{equation}
where the quadratic covariation 
\begin{equation}
[X_t, Y_t] = \int_0^t dX_s dY_s = \lim_{\|P\| \rightarrow 0} \sum_{k=1}^n  (X_{t_k} - X_{t_{k-1}})(Y_{t_k} - Y_{t_{k-1}})
\end{equation}
where $P$ is the mesh ranges over partitions of interval $[0, t]$ and the norm of $P$ is the mesh size. 
\end{concept}

\begin{concept}{Quadratic Variation of Wiener Process: $dW \cdot dW = dt$}
The quadratic variation of Wienier process is 
\begin{equation}
[W_t] = \int_0^t (dW_s)^2 = \lim_{n \rightarrow \infty} 
        \sum_{k=1}^n (W_{\frac{tk}{n}} - W_{\frac{t(k-1)}{n}})^2 = t 
\end{equation}
Let's define a standard normal variable $Z_{nk} \sim N(0, 1)$ :
\begin{equation}
Z_{nk} = \frac{(W_{\frac{tk}{n}} - W_{\frac{t(k-1)}{n}})}{\sqrt{t/n}}
\end{equation}
We have
\begin{equation}
[W_t] = \lim_{n \rightarrow \infty} t\sum_{k=1}^n \frac{Z_{nk}^2}{n} = t \E[Z_{nk}^2] = t
\end{equation}
Therefore we have $\int_0^t(dW_s)^2 = t$ and $dW\cdot dW = dt$. The non-zero quadratic variation is where Ito calculus differs from ordinary calculus.
\end{concept}

\begin{concept}{It\^{o}'s Lemma: Higher Dimensions}
\begin{equation}
d f(t, \vec{X}) = \frac{\partial f}{\partial t} dt + (\nabla_{\vec{X}} f) d\vec{X} 
+ \frac{1}{2} d \vec{X}^{\top} (\vec{H}_{\vec{X}} f) d \vec{X}
\end{equation}
\end{concept}




 
\newpage